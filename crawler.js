(async () => {
    const origin = document.location.origin;
    const parser = new DOMParser();
    let queue = [document.location.href];
    let crawl = [];
    let stop = false;

    const model = {
        URL: '',
        Status: 0,
        'Content-type': '',
        'Meta robots': '',
        Title: '',
        'Meta description': '',
        H1: ''
    };

    document.write(
        `<style>table{border-collapse: collapse}td,th{border:1px solid #ddd}</style><button>pause/start</button><table><tbody><tr>${Object.keys(model)
            .map((k) => `<th>${k}</th>`)
            .join('')}</tr></tbody></table>`
    );

    await step();

    async function step() {
        const page = { ...model };
        page.URL = queue.pop();
        crawl.push(page);

        document.querySelector('button').onclick = () => {
            if (!stop) stop = true;
            else {
                stop = false;
                step();
            }
        };

        try {
            const response = await fetch(page.URL);
            page.Status = response.status;
            page['Content-type'] = response.headers.get('content-type').split(';')[0].toLocaleLowerCase().trim();

            if (page.Status == 200 && page['Content-type'] == 'text/html') {
                const string = await response.text();
                const dom = parser.parseFromString(string, 'text/html');

                page.Title = dom.title ? dom.title : '';
                page.H1 = dom.querySelector('h1') ? dom.querySelector('h1').textContent : '';

                const metaTags = Array.from(dom.querySelectorAll('meta[name][content]'));

                page['Meta description'] = metaTags.find((meta) => meta.name.match(/description/i)).content;

                page['Meta robots'] = metaTags.find((meta) => meta.name.match(/robots/i)).content;

                const links = Array.from(dom.querySelectorAll('a'))
                    .map((a) => a.href.replace(/#.*/, ''))
                    .forEach((href) => {
                        if (queue.indexOf(href) == -1 && !crawl.find((page) => page.URL == href) && href.startsWith(origin)) queue.push(href);
                    });
            }
        } catch (error) {
            console.error;
        }

        const tr = document.createElement('tr');
        tr.innerHTML = Object.values(page)
            .map((v) => `<td>${v}</td>`)
            .join('');

        document.querySelector('tbody').appendChild(tr);

        if (queue.length > 0 && !stop) await step();
    }
})();
